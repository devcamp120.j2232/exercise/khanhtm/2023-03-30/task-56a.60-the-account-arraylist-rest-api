package com.devcamp.s50.taskjbr360.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountControl {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount(){
       Account account1 = new Account("01", "Khanh", 40000); 
       Account account2 = new Account("02", "Hoàng", 20000); 
       Account account3 = new Account("03", "Ngọc", 70000);
       System.out.println(account1.toString()); 
       System.out.println(account2.toString()); 
       System.out.println(account3.toString());
       ArrayList<Account> accounts = new ArrayList<>();
       accounts.add(account1);
       accounts.add(account2);
       accounts.add(account3);
       return accounts;
    }
}
